namespace VeterinaryClinic.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema; 

    [Table("Appointment")]
    public partial class Appointment
    {
        [Key] 
        public int IdAppointment { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombre cita")]
        public string NameAppointment { get; set; }

        [Column(TypeName = "date")]
        [DisplayName("Inicio Cita")]
        public DateTime DateStartAppointment { get; set; }


        [Column(TypeName = "date")]
        [DisplayName("Fin cita")]
        public DateTime DateEndAppointment { get; set; }

        [DisplayName("Paciente")]
        public int IdPatient { get; set; }

        [DisplayName("Salon")]
        public int IdRoom { get; set; }
        [DisplayName("Empleado")]

        public int IdEmployee { get; set; }

        [ForeignKey("IdEmployee")]
        public virtual Employee Employee { get; set; }

        [ForeignKey("IdPatient")]
        public virtual Patient Patient { get; set; }

        [ForeignKey("IdRoom")]
        public virtual Room Room { get; set; }
    }
}
