namespace VeterinaryClinic.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Rol")]
    public partial class Rol
    {
          public Rol()
        {
            Employee = new HashSet<Employee>();
        }

        [Key]
        public int IdRol { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombre Rol")]
        public string NameRol { get; set; }

        [Column(TypeName = "text")]

        [DisplayName("Descripción")]
        public string Description { get; set; }

         public virtual ICollection<Employee> Employee { get; set; }
    }
}
