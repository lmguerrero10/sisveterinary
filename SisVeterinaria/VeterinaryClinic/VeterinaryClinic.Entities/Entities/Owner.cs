namespace VeterinaryClinic.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Owner")]
    public partial class Owner
    {
         public Owner()
        {
            Patient = new HashSet<Patient>();
        }

        [Key]
        public int IdOwner { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Nombre propietario")]
        public string NameOwner { get; set; }

        [DisplayName("# Documento")]
        public int DocumentOwner { get; set; }

        [StringLength(50)]
        [DisplayName("Telefono")]
        public string Telphone { get; set; }

        [StringLength(50)]
        [DisplayName("Correo")]
        public string Email { get; set; }

        [DisplayName("Tipo de documento")]
        public int IdDocumentType { get; set; }

        [ForeignKey("IdDocumentType")]
        public virtual DocumentType DocumentType { get; set; }
        public virtual ICollection<Patient> Patient { get; set; }
    }
}
