namespace VeterinaryClinic.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Patient")]
    public partial class Patient
    {
         public Patient()
        {
            Appointment = new HashSet<Appointment>();
        }

        [Key] 
        public int IdPatient { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Nombre Paciente")]
        public string NamePatient { get; set; }

        [DisplayName("Propietario")]
        public int IdOwner { get; set; }

        [DisplayName("Especie")]
        public int IdSpecie { get; set; }

        [DisplayName("Raza")]
        public int IdRace { get; set; }

        [DisplayName("Edad")]
        public int? Age { get; set; }

        [Column(TypeName = "date")]
        [DisplayName("Fecha registro")]
        public DateTime? DateRegistration { get; set; }

          public virtual ICollection<Appointment> Appointment { get; set; }



        [ForeignKey("IdOwner")]
        public virtual Owner Owner { get; set; }

        [ForeignKey("IdRace")]
        public virtual Race Race { get; set; }


        [ForeignKey("IdSpecie")]
        public virtual Specie Specie { get; set; }
    }
}
