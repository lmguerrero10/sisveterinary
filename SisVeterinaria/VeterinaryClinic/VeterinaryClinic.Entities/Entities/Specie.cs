namespace VeterinaryClinic.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Specie")]
    public partial class Specie
    {
        public Specie()
        {
            Patient = new HashSet<Patient>();
        }

        [Key] 
        public int IdSpecie { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Especie")]
        public string NameSpecie { get; set; }

        [DisplayName("Descripción")]
        public string Description { get; set; }

       public virtual ICollection<Patient> Patient { get; set; }
    }
}
