namespace VeterinaryClinic.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("WorkStation")]
    public partial class WorkStation
    {
         public WorkStation()
        {
            Employee = new HashSet<Employee>();
        }

        [Key]
        public int IdWorkStation { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Area de trabajo")]
        public string NameWorkStation { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateCreate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateUpdate { get; set; } 

        public virtual ICollection<Employee> Employee { get; set; }
    }
}
