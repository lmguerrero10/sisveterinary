namespace VeterinaryClinic.Entities.Entities
{ 
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;  

    [Table("Employee")]
    public partial class Employee
    {
         public Employee()
        {
            Appointment = new HashSet<Appointment>();
        }

        [Key] 
        public int IdEmployee { get; set; }

        [Required]
        [StringLength(50)]

        [DisplayName("Nombre empleado")]
        public string NameEmployee { get; set; }

        [DisplayName("Documento")]
        public int DocumentEmployee { get; set; }

        [DisplayName("Tipo Documento")]
        public int IdDocumentType { get; set; }

        [DisplayName("Especialidad")]

        public int Idspecialty { get; set; }

        [StringLength(50)]
        [DisplayName("Telefono")]
        public string Telphone { get; set; }

        [StringLength(100)]
        [DisplayName("Correo")]
        public string Email { get; set; }

        [DisplayName("Rol")]
        public int IdRol { get; set; }

        [DisplayName("Area de trabajo")]
        public int IdWorkStation { get; set; }

        [Column(TypeName = "date")]
        [DisplayName("Fecha creaci�n")]
        public DateTime DateCreate { get; set; }


        [Column(TypeName = "date")]
        [DisplayName("Fecha actualizacion")]
        public DateTime? DateUpdate { get; set; }

        public virtual ICollection<Appointment> Appointment { get; set; }

        [ForeignKey("IdDocumentType")]
        public virtual DocumentType DocumentType { get; set; }


        [ForeignKey("IdRol")]
        public virtual Rol Rol { get; set; }

        [ForeignKey("IdSpecialty")]
        public virtual Specialty Specialty { get; set; }

        [ForeignKey("IdWorkStation")]
        public virtual WorkStation WorkStation { get; set; }
    }
}
