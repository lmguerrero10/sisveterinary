namespace VeterinaryClinic.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema; 

    [Table("DocumentType")]
    public partial class DocumentType
    {
       

        [Key]
        public int IdDocumentType { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombre documento")]
        public string NameDocumentType { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<Owner> Owner { get; set; }
    }
}
