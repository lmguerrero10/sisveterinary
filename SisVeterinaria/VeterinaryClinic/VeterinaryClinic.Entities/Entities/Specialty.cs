namespace VeterinaryClinic.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Specialty")]
    public partial class Specialty
    {
         public Specialty()
        {
            Employee = new HashSet<Employee>();
        }

        [Key] 
        public int Idspecialty { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombre Especialidad")]
        public string Namespecialty { get; set; }

        [Column(TypeName = "date")]
        [DisplayName("Fecha Creaci�n")]
        public DateTime? DateCreate { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
    }
}
