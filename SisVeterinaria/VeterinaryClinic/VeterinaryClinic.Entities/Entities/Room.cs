namespace VeterinaryClinic.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Room")]
    public partial class Room
    {
        public Room()
        {
            Appointment = new HashSet<Appointment>();
        }

        [Key]
        public int IdRoom { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombre salon-aula")]
        public string NameRoom { get; set; }

        [Column(TypeName = "text")]
        [DisplayName("Detalle")]
        public string Description { get; set; }

        [DisplayName("┐Disponible?")]

        public bool IsActive { get; set; }
         
       public virtual ICollection<Appointment> Appointment { get; set; }
    }
}
