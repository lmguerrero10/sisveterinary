namespace VeterinaryClinic.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Race")]
    public partial class Race
    {
         public Race()
        {
            Patient = new HashSet<Patient>();
        }

        [Key] 
        public int IdRace { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombre raza")]
        public string NameRace { get; set; }

        [DisplayName("Descripcion")]
        public string Description { get; set; }

         public virtual ICollection<Patient> Patient { get; set; }
    }
}
