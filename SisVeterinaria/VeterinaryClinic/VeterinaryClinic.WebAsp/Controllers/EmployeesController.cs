﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VeterinaryClinic.WebAsp.Entities;

namespace VeterinaryClinic.WebAsp.Controllers
{
    public class EmployeesController : Controller
    {
        private VeterinaryClinicContext db = new VeterinaryClinicContext();

        // GET: Employees
        public async Task<ActionResult> Index()
        {
            var employee = db.Employee.Include(e => e.DocumentType).Include(e => e.Rol).Include(e => e.Specialty).Include(e => e.WorkStation);
            return View(await employee.ToListAsync());
        }

        // GET: Employees/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = await db.Employee.FindAsync(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            ViewBag.IdDocumentType = new SelectList(db.DocumentType, "IdDocumentType", "NameDocumentType");
            ViewBag.IdRol = new SelectList(db.Rol, "IdRol", "NameRol");
            ViewBag.Idspecialty = new SelectList(db.Specialty, "Idspecialty", "Namespecialty");
            ViewBag.IdWorkStation = new SelectList(db.WorkStation, "IdWorkStation", "NameWorkStation");
            return View();
        }

        // POST: Employees/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdEmployee,NameEmployee,DocumentEmployee,IdDocumentType,Idspecialty,Telphone,Email,IdRol,IdWorkStation,DateCreate,DateUpdate")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employee.Add(employee);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IdDocumentType = new SelectList(db.DocumentType, "IdDocumentType", "NameDocumentType", employee.IdDocumentType);
            ViewBag.IdRol = new SelectList(db.Rol, "IdRol", "NameRol", employee.IdRol);
            ViewBag.Idspecialty = new SelectList(db.Specialty, "Idspecialty", "Namespecialty", employee.Idspecialty);
            ViewBag.IdWorkStation = new SelectList(db.WorkStation, "IdWorkStation", "NameWorkStation", employee.IdWorkStation);
            return View(employee);
        }

        // GET: Employees/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = await db.Employee.FindAsync(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdDocumentType = new SelectList(db.DocumentType, "IdDocumentType", "NameDocumentType", employee.IdDocumentType);
            ViewBag.IdRol = new SelectList(db.Rol, "IdRol", "NameRol", employee.IdRol);
            ViewBag.Idspecialty = new SelectList(db.Specialty, "Idspecialty", "Namespecialty", employee.Idspecialty);
            ViewBag.IdWorkStation = new SelectList(db.WorkStation, "IdWorkStation", "NameWorkStation", employee.IdWorkStation);
            return View(employee);
        }

        // POST: Employees/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdEmployee,NameEmployee,DocumentEmployee,IdDocumentType,Idspecialty,Telphone,Email,IdRol,IdWorkStation,DateCreate,DateUpdate")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IdDocumentType = new SelectList(db.DocumentType, "IdDocumentType", "NameDocumentType", employee.IdDocumentType);
            ViewBag.IdRol = new SelectList(db.Rol, "IdRol", "NameRol", employee.IdRol);
            ViewBag.Idspecialty = new SelectList(db.Specialty, "Idspecialty", "Namespecialty", employee.Idspecialty);
            ViewBag.IdWorkStation = new SelectList(db.WorkStation, "IdWorkStation", "NameWorkStation", employee.IdWorkStation);
            return View(employee);
        }

        // GET: Employees/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = await db.Employee.FindAsync(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Employee employee = await db.Employee.FindAsync(id);
            db.Employee.Remove(employee);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
