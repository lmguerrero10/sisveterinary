﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VeterinaryClinic.WebAsp.Entities;

namespace VeterinaryClinic.WebAsp.Controllers
{
    public class PatientsController : Controller
    {
        private VeterinaryClinicContext db = new VeterinaryClinicContext();

        // GET: Patients
        public async Task<ActionResult> Index()
        {
            var patient = db.Patient.Include(p => p.Owner).Include(p => p.Race).Include(p => p.Specie);
            return View(await patient.ToListAsync());
        }

        // GET: Patients/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Patient patient = await db.Patient.FindAsync(id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            return View(patient);
        }

        // GET: Patients/Create
        public ActionResult Create()
        {
            ViewBag.IdOwner = new SelectList(db.Owner, "IdOwner", "NameOwner");
            ViewBag.IdRace = new SelectList(db.Race, "IdRace", "NameRace");
            ViewBag.IdSpecie = new SelectList(db.Specie, "IdSpecie", "NameSpecie");
            return View();
        }

        // POST: Patients/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdPatient,NamePatient,IdOwner,IdSpecie,IdRace,Age,DateRegistration")] Patient patient)
        {
            if (ModelState.IsValid)
            {
                db.Patient.Add(patient);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IdOwner = new SelectList(db.Owner, "IdOwner", "NameOwner", patient.IdOwner);
            ViewBag.IdRace = new SelectList(db.Race, "IdRace", "NameRace", patient.IdRace);
            ViewBag.IdSpecie = new SelectList(db.Specie, "IdSpecie", "NameSpecie", patient.IdSpecie);
            return View(patient);
        }

        // GET: Patients/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Patient patient = await db.Patient.FindAsync(id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdOwner = new SelectList(db.Owner, "IdOwner", "NameOwner", patient.IdOwner);
            ViewBag.IdRace = new SelectList(db.Race, "IdRace", "NameRace", patient.IdRace);
            ViewBag.IdSpecie = new SelectList(db.Specie, "IdSpecie", "NameSpecie", patient.IdSpecie);
            return View(patient);
        }

        // POST: Patients/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdPatient,NamePatient,IdOwner,IdSpecie,IdRace,Age,DateRegistration")] Patient patient)
        {
            if (ModelState.IsValid)
            {
                db.Entry(patient).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IdOwner = new SelectList(db.Owner, "IdOwner", "NameOwner", patient.IdOwner);
            ViewBag.IdRace = new SelectList(db.Race, "IdRace", "NameRace", patient.IdRace);
            ViewBag.IdSpecie = new SelectList(db.Specie, "IdSpecie", "NameSpecie", patient.IdSpecie);
            return View(patient);
        }

        // GET: Patients/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Patient patient = await db.Patient.FindAsync(id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            return View(patient);
        }

        // POST: Patients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Patient patient = await db.Patient.FindAsync(id);
            db.Patient.Remove(patient);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
