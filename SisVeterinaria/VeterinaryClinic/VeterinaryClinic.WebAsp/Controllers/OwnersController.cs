﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VeterinaryClinic.WebAsp.Entities;

namespace VeterinaryClinic.WebAsp.Controllers
{
    public class OwnersController : Controller
    {
        private VeterinaryClinicContext db = new VeterinaryClinicContext();

        // GET: Owners
        public async Task<ActionResult> Index()
        {
            var owner = db.Owner.Include(o => o.DocumentType);
            return View(await owner.ToListAsync());
        }

        // GET: Owners/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Owner owner = await db.Owner.FindAsync(id);
            if (owner == null)
            {
                return HttpNotFound();
            }
            return View(owner);
        }

        // GET: Owners/Create
        public ActionResult Create()
        {
            ViewBag.IdDocumentType = new SelectList(db.DocumentType, "IdDocumentType", "NameDocumentType");
            return View();
        }

        // POST: Owners/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdOwner,NameOwner,DocumentOwner,Telphone,Email,IdDocumentType")] Owner owner)
        {
            if (ModelState.IsValid)
            {
                db.Owner.Add(owner);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IdDocumentType = new SelectList(db.DocumentType, "IdDocumentType", "NameDocumentType", owner.IdDocumentType);
            return View(owner);
        }

        // GET: Owners/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Owner owner = await db.Owner.FindAsync(id);
            if (owner == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdDocumentType = new SelectList(db.DocumentType, "IdDocumentType", "NameDocumentType", owner.IdDocumentType);
            return View(owner);
        }

        // POST: Owners/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdOwner,NameOwner,DocumentOwner,Telphone,Email,IdDocumentType")] Owner owner)
        {
            if (ModelState.IsValid)
            {
                db.Entry(owner).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IdDocumentType = new SelectList(db.DocumentType, "IdDocumentType", "NameDocumentType", owner.IdDocumentType);
            return View(owner);
        }

        // GET: Owners/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Owner owner = await db.Owner.FindAsync(id);
            if (owner == null)
            {
                return HttpNotFound();
            }
            return View(owner);
        }

        // POST: Owners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Owner owner = await db.Owner.FindAsync(id);
            db.Owner.Remove(owner);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
