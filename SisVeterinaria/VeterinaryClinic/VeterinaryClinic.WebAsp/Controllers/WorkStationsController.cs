﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VeterinaryClinic.WebAsp.Entities;

namespace VeterinaryClinic.WebAsp.Controllers
{
    public class WorkStationsController : Controller
    {
        private VeterinaryClinicContext db = new VeterinaryClinicContext();

        // GET: WorkStations
        public async Task<ActionResult> Index()
        {
            return View(await db.WorkStation.ToListAsync());
        }

        // GET: WorkStations/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkStation workStation = await db.WorkStation.FindAsync(id);
            if (workStation == null)
            {
                return HttpNotFound();
            }
            return View(workStation);
        }

        // GET: WorkStations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WorkStations/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdWorkStation,NameWorkStation,DateCreate,DateUpdate")] WorkStation workStation)
        {
            if (ModelState.IsValid)
            {
                db.WorkStation.Add(workStation);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(workStation);
        }

        // GET: WorkStations/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkStation workStation = await db.WorkStation.FindAsync(id);
            if (workStation == null)
            {
                return HttpNotFound();
            }
            return View(workStation);
        }

        // POST: WorkStations/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdWorkStation,NameWorkStation,DateCreate,DateUpdate")] WorkStation workStation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(workStation).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(workStation);
        }

        // GET: WorkStations/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkStation workStation = await db.WorkStation.FindAsync(id);
            if (workStation == null)
            {
                return HttpNotFound();
            }
            return View(workStation);
        }

        // POST: WorkStations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            WorkStation workStation = await db.WorkStation.FindAsync(id);
            db.WorkStation.Remove(workStation);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
