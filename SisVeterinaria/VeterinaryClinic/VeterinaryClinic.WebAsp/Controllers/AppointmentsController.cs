﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VeterinaryClinic.WebAsp.Entities;

namespace VeterinaryClinic.WebAsp.Controllers
{
    public class AppointmentsController : Controller
    {
        private VeterinaryClinicContext db = new VeterinaryClinicContext();

        // GET: Appointments
        public async Task<ActionResult> Index()
        {
            var appointment = db.Appointment.Include(a => a.Employee).Include(a => a.Patient).Include(a => a.Room);
            return View(await appointment.ToListAsync());
        }

        // GET: Appointments/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = await db.Appointment.FindAsync(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }
            return View(appointment);
        }

        // GET: Appointments/Create
        public ActionResult Create()
        {
            ViewBag.IdEmployee = new SelectList(db.Employee, "IdEmployee", "NameEmployee");
            ViewBag.IdPatient = new SelectList(db.Patient, "IdPatient", "NamePatient");
            ViewBag.IdRoom = new SelectList(db.Room, "IdRoom", "NameRoom");
            return View();
        }

        // POST: Appointments/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdAppointment,NameAppointment,DateStartAppointment,DateEndAppointment,IdPatient,IdRoom,IdEmployee")] Appointment appointment)
        {
            if (ModelState.IsValid)
            {
                db.Appointment.Add(appointment);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IdEmployee = new SelectList(db.Employee, "IdEmployee", "NameEmployee", appointment.IdEmployee);
            ViewBag.IdPatient = new SelectList(db.Patient, "IdPatient", "NamePatient", appointment.IdRoom);
            ViewBag.IdRoom = new SelectList(db.Room, "IdRoom", "NameRoom", appointment.IdRoom);
            return View(appointment);
        }

        // GET: Appointments/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = await db.Appointment.FindAsync(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdEmployee = new SelectList(db.Employee, "IdEmployee", "NameEmployee", appointment.IdEmployee);
            ViewBag.IdRoom = new SelectList(db.Patient, "IdPatient", "NamePatient", appointment.IdRoom);
            ViewBag.IdRoom = new SelectList(db.Room, "IdRoom", "NameRoom", appointment.IdRoom);
            return View(appointment);
        }

        // POST: Appointments/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdAppointment,NameAppointment,DateStartAppointment,DateEndAppointment,IdPatient,IdRoom,IdEmployee")] Appointment appointment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(appointment).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IdEmployee = new SelectList(db.Employee, "IdEmployee", "NameEmployee", appointment.IdEmployee);
            ViewBag.IdRoom = new SelectList(db.Patient, "IdPatient", "NamePatient", appointment.IdRoom);
            ViewBag.IdRoom = new SelectList(db.Room, "IdRoom", "NameRoom", appointment.IdRoom);
            return View(appointment);
        }

        // GET: Appointments/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = await db.Appointment.FindAsync(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }
            return View(appointment);
        }

        // POST: Appointments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Appointment appointment = await db.Appointment.FindAsync(id);
            db.Appointment.Remove(appointment);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
