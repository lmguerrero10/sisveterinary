namespace VeterinaryClinic.WebAsp.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Patient")]
    public partial class Patient
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Patient()
        {
            Appointment = new HashSet<Appointment>();
        }

        [Key]
        public int IdPatient { get; set; }

        [Required]
        [StringLength(100)]
        public string NamePatient { get; set; }

        public int IdOwner { get; set; }

        public int IdSpecie { get; set; }

        public int IdRace { get; set; }

        public int? Age { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateRegistration { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Appointment> Appointment { get; set; }

        public virtual Owner Owner { get; set; }

        public virtual Race Race { get; set; }

        public virtual Specie Specie { get; set; }
    }
}
