namespace VeterinaryClinic.WebAsp.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DocumentType")]
    public partial class DocumentType
    {
         public DocumentType()
        {
            Employee = new HashSet<Employee>();
            Owner = new HashSet<Owner>();
        }

        [Key]
        public int IdDocumentType { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name ="Nombre")]
        public string NameDocumentType { get; set; } 
        public virtual ICollection<Employee> Employee { get; set; } 
        public virtual ICollection<Owner> Owner { get; set; }
    }
}
