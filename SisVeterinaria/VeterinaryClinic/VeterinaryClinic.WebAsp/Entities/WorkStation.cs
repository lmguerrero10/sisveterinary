namespace VeterinaryClinic.WebAsp.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WorkStation")]
    public partial class WorkStation
    { 
        public WorkStation()
        {
            Employee = new HashSet<Employee>();
        }

        [Key]
        public int IdWorkStation { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Nombre")]
        public string NameWorkStation { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateCreate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateUpdate { get; set; }
         
        public virtual ICollection<Employee> Employee { get; set; }
    }
}
