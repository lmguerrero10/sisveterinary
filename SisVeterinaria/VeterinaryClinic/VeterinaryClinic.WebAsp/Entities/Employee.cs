namespace VeterinaryClinic.WebAsp.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            Appointment = new HashSet<Appointment>();
        }

        [Key]
        public int IdEmployee { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Nombre")]
        public string NameEmployee { get; set; }
        [Display(Name = "Documento")]
        public int DocumentEmployee { get; set; }

        [Display(Name = "Tipo Documento")]
        public int IdDocumentType { get; set; }
        [Display(Name = "Especialidad")]
        public int Idspecialty { get; set; }

        [StringLength(50)]
        [Display(Name = "Telefono")]
        public string Telphone { get; set; }

        [StringLength(100)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Rol")]
        public int IdRol { get; set; }
        [Display(Name = "Area")]

        public int IdWorkStation { get; set; }

        [Column(TypeName = "date")]

        public DateTime DateCreate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateUpdate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Appointment> Appointment { get; set; }

        public virtual DocumentType DocumentType { get; set; }

        public virtual Rol Rol { get; set; }

        public virtual Specialty Specialty { get; set; }

        public virtual WorkStation WorkStation { get; set; }
    }
}
