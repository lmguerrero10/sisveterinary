namespace VeterinaryClinic.WebAsp.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Room")]
    public partial class Room
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Room()
        {
            Appointment = new HashSet<Appointment>();
        }

        [Key]
        public int IdRoom { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Nombre")]
        public string NameRoom { get; set; }

        [Column(TypeName = "text")]
        [Display(Name = "Detalle")]
        public string Description { get; set; }
        [Display(Name = "┐Disponible?")]
        public bool IsActive { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Appointment> Appointment { get; set; }
    }
}
