using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace VeterinaryClinic.WebAsp.Entities
{
    public partial class VeterinaryClinicContext : DbContext
    {
        public VeterinaryClinicContext()
            : base("name=VeterinaryClinicContext")
        {
        }

        public virtual DbSet<Appointment> Appointment { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Owner> Owner { get; set; }
        public virtual DbSet<Patient> Patient { get; set; }
        public virtual DbSet<Race> Race { get; set; }
        public virtual DbSet<Rol> Rol { get; set; }
        public virtual DbSet<Room> Room { get; set; }
        public virtual DbSet<Specialty> Specialty { get; set; }
        public virtual DbSet<Specie> Specie { get; set; }
        public virtual DbSet<WorkStation> WorkStation { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Appointment>()
                .Property(e => e.NameAppointment)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .Property(e => e.NameDocumentType)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .HasMany(e => e.Employee)
                .WithRequired(e => e.DocumentType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DocumentType>()
                .HasMany(e => e.Owner)
                .WithRequired(e => e.DocumentType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.NameEmployee)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Telphone)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Appointment)
                .WithRequired(e => e.Employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Owner>()
                .Property(e => e.NameOwner)
                .IsUnicode(false);

            modelBuilder.Entity<Owner>()
                .Property(e => e.Telphone)
                .IsUnicode(false);

            modelBuilder.Entity<Owner>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Owner>()
                .HasMany(e => e.Patient)
                .WithRequired(e => e.Owner)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.NamePatient)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .HasMany(e => e.Appointment)
                .WithRequired(e => e.Patient)
                .HasForeignKey(e => e.IdRoom)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Race>()
                .Property(e => e.NameRace)
                .IsUnicode(false);

            modelBuilder.Entity<Race>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Race>()
                .HasMany(e => e.Patient)
                .WithRequired(e => e.Race)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rol>()
                .Property(e => e.NameRol)
                .IsUnicode(false);

            modelBuilder.Entity<Rol>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Rol>()
                .HasMany(e => e.Employee)
                .WithRequired(e => e.Rol)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Room>()
                .Property(e => e.NameRoom)
                .IsUnicode(false);

            modelBuilder.Entity<Room>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Room>()
                .HasMany(e => e.Appointment)
                .WithRequired(e => e.Room)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Specialty>()
                .Property(e => e.Namespecialty)
                .IsUnicode(false);

            modelBuilder.Entity<Specialty>()
                .HasMany(e => e.Employee)
                .WithRequired(e => e.Specialty)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Specie>()
                .Property(e => e.NameSpecie)
                .IsUnicode(false);

            modelBuilder.Entity<Specie>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Specie>()
                .HasMany(e => e.Patient)
                .WithRequired(e => e.Specie)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WorkStation>()
                .Property(e => e.NameWorkStation)
                .IsUnicode(false);

            modelBuilder.Entity<WorkStation>()
                .HasMany(e => e.Employee)
                .WithRequired(e => e.WorkStation)
                .WillCascadeOnDelete(false);
        }
    }
}
