namespace VeterinaryClinic.WebAsp.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Owner")]
    public partial class Owner
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Owner()
        {
            Patient = new HashSet<Patient>();
        }

        [Key]
        public int IdOwner { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Nombre")]
        public string NameOwner { get; set; }

        [Display(Name = "Documento")]
        public int DocumentOwner { get; set; }

        [StringLength(50)]
        [Display(Name = "Telefono")]
        public string Telphone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [Display(Name = "Tipo Documento")]
        public int IdDocumentType { get; set; }

        public virtual DocumentType DocumentType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Patient> Patient { get; set; }
    }
}
