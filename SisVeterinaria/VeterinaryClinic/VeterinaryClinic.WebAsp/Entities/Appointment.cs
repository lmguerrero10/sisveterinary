namespace VeterinaryClinic.WebAsp.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Appointment")]
    public partial class Appointment
    {
        [Key]
        public int IdAppointment { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Nombre")]
        public string NameAppointment { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Fecha Inicio")]
        public DateTime DateStartAppointment { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Fecha Fin")]
        public DateTime DateEndAppointment { get; set; }

        [Display(Name = "Paciente")]
        public int IdPatient { get; set; }

        [Display(Name = "Sal�n")]
        public int IdRoom { get; set; }

        [Display(Name = "Medico")]
        public int IdEmployee { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Patient Patient { get; set; }

        public virtual Room Room { get; set; }
    }
}
