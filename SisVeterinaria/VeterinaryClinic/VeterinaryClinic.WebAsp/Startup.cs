﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VeterinaryClinic.WebAsp.Startup))]
namespace VeterinaryClinic.WebAsp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
