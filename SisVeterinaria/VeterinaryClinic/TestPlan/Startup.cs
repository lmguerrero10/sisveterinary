﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestPlan.Startup))]
namespace TestPlan
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
