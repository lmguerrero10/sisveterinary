namespace TestPlan.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Appointment")]
    public partial class Appointment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdAppointment { get; set; }

        [Required]
        [StringLength(50)]
        public string NameAppointment { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateStartAppointment { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateEndAppointment { get; set; }

        public int IdPatient { get; set; }

        public int IdRoom { get; set; }

        public int IdEmployee { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Patient Patient { get; set; }

        public virtual Room Room { get; set; }
    }
}
