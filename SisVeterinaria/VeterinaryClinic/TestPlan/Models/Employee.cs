namespace TestPlan.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            Appointment = new HashSet<Appointment>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdEmployee { get; set; }

        [Required]
        [StringLength(50)]
        public string NameEmployee { get; set; }

        public int DocumentEmployee { get; set; }

        public int IdDocumentType { get; set; }

        public int Idspecialty { get; set; }

        [StringLength(50)]
        public string Telphone { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public int IdRol { get; set; }

        public int IdWorkStation { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateCreate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateUpdate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Appointment> Appointment { get; set; }

        public virtual DocumentType DocumentType { get; set; }

        public virtual Rol Rol { get; set; }

        public virtual Specialty Specialty { get; set; }

        public virtual WorkStation WorkStation { get; set; }
    }
}
